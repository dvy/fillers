import re
import pymorphy2

def split_to_sentences(text):
	text = text.replace('\n', '')
	sentences = re.split(r' *[\.\?!][\'"\)\]]* *', text)
	return sentences

def split_to_words(text):
	text = text.replace('\n', '')
	tokens = re.split('\W+', text)
	return tokens

def average_sentence_length(sents):
	avg_len = sum(len(word.split()) for word in sents) / len(sents)
	return avg_len
